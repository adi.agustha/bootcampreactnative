const golden = () => {
    console.log("this is golden!!")
  }
  
  golden()
//==============
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        }
    }
} 
newFunction("William", "Imoh").fullName()

//==============
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  
  const { firstName, lastName, destination, occupation, spell } = newObject
  
  console.log(firstName, lastName, destination, occupation, spell)

  
  