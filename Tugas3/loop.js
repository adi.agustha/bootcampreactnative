//LOOPING WHILE
//Looping Pertama
var number = 2
var word = " i love coding"
while(number < 20) { // Loop akan terus berjalan selama number masih dibawah 20
    console.log(number + word); // kalimat
    number +=2; // Menambah number dengan menambahkan 2
  }
//Looping Kedua
var number_2 = 20
var word_2 = " I will become mobile developer"
while(number_2 > 0) { // Loop akan terus berjalan selama number masih dibawah 20
    console.log(number_2 + word_2); // kalimat
    number_2 -=2; // Mengurangi number dengan mengurangi 2
}

//Looping Ketiga

for (var x=1; x<=20; x++) {
  if (x % 2 != 0) {
      console.log(x +  " - Santai");
  }
  if (x % 2 == 0) {
      console.log(x + " - Berkualitas");   
  }
  else if (x % 3 == 0) 
      console.log(x + " - I Love Coding");
  }
  